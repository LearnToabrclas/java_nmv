/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lamda;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PC
 */
public class Book_Use_Lamda {
   public static void main(String[] args) {
        // Tạo danh sách sách
        List<Book> books = new ArrayList<>();
        books.add(new Book("truyen tranh", "Nguyen van a", 1925));
        books.add(new Book("tieu thuyet", "kinhkihu", 1960));
        books.add(new Book("1977", "nemon", 1949));
        books.add(new Book("ngan le 1 dem", "viet nam anh ", 1200));

        // Sử dụng biểu thức Lambda để lọc các sách được xuất bản sau năm 1950
        List<Book> filteredBooks = filterBooks(books, book -> book.getYear() > 1950);

        // Hiển thị danh sách sách đã lọc
        System.out.println("Filtered Books:");
        for (Book book : filteredBooks) {
            System.out.println(book.getTitle() + " by " + book.getAuthor() + " (" + book.getYear() + ")");
        }
    }

    public static List<Book> filterBooks(List<Book> books, filBook filter) {
        List<Book> filteredBooks = new ArrayList<>();
        for (Book book : books) {
            if (filter.test(book)) {
                filteredBooks.add(book);
            }
        }
        return filteredBooks;
    } 
}

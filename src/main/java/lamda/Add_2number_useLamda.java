/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lamda;

/**
 *
 * @author PC
 */
public class Add_2number_useLamda {

    public static void main(String[] args) {
        // Sử dụng biểu thức Lambda để cộng hai số
        Calculator calculator = (a, b) -> a + b;

        int result = calculator.add(5, 3);
        System.out.println("Result: " + result);
    }
}

interface Calculator {

    int add(int a, int b);

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package encapsulationinjava;

/**
 *
 * @author PC
 */
public class Computer {
    private String brand;
    private String model;
    private int ramSize;
    private int storageSize;

    public Computer(String brand, String model, int ramSize, int storageSize) {
        this.brand = brand;
        this.model = model;
        this.ramSize = ramSize;
        this.storageSize = storageSize;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public int getRamSize() {
        return ramSize;
    }

    public int getStorageSize() {
        return storageSize;
    }

    public void upgradeRam(int newRamSize) {
        this.ramSize = newRamSize;
    }

    public void upgradeStorage(int newStorageSize) {
        this.storageSize = newStorageSize;
    }

    public void displayInfo() {
        System.out.println("Brand: " + brand);
        System.out.println("Model: " + model);
        System.out.println("RAM Size: " + ramSize + " GB");
        System.out.println("Storage Size: " + storageSize + " GB");
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buoih9;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PC
 */
public class ArrayList_BTVN {

    public static void main(String[] args) {
        //create an arraylist 
        List<Integer> numbersInaArrayList = new ArrayList<>(4);
        //and then add ,delete 
        //Add for ArrayList:
        numbersInaArrayList.add(1);
        numbersInaArrayList.add(2);
        numbersInaArrayList.add(5);
        numbersInaArrayList.add(9);
        System.out.println("All element in List is:" + numbersInaArrayList.toString());
        //Delete
        numbersInaArrayList.remove(Integer.valueOf(9));
        System.out.println("ArrayList After delete element 9:" + numbersInaArrayList.toString());
        //check if number exist=>remove else we displays:No have in list
        int elementRemove = 2;
        if (numbersInaArrayList.contains(elementRemove)) {
            numbersInaArrayList.remove(Integer.valueOf(elementRemove));
            System.out.println("had delete " + elementRemove);
        } else {
            System.out.println("No have in list!");
        }
        //and edit elements in it  
        numbersInaArrayList.set(0, 100);
        System.out.println("Array after edit:" + numbersInaArrayList.toString());
    }
}

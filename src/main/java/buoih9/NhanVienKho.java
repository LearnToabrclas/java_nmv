/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buoih9;

/**
 *
 * @author PC
 */
public class NhanVienKho extends NhanVienVanPhong implements WorkBenefit{
    private String tinhTrangSucKhoe;

    public NhanVienKho(String tinhTrangSucKhoe, String totNghiepTruong, String queQuan) {
        super(totNghiepTruong, queQuan);
        this.tinhTrangSucKhoe = tinhTrangSucKhoe;
    }
     public String getTinhTrangSucKhoe() {
        return tinhTrangSucKhoe;
    }

    public void setTinhTrangSucKhoe(String tinhTrangSucKhoe) {
        this.tinhTrangSucKhoe = tinhTrangSucKhoe;
    }

    @Override
    public String getWorkBenefit() {
      return "Chi tra chi khi kham va chua benh";
    }

}

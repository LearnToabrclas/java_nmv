/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buoih9;

/**
 *
 * @author PC
 */
public class NhanVienBanHang extends NhanVienVanPhong implements WorkBenefit{

    public NhanVienBanHang(String totNghiepTruong, String queQuan) {
        super(totNghiepTruong, queQuan);
    }
    
    @Override
    public String getWorkBenefit() {
        return "chi tra tien an trua va dien thoai";
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buoih9;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PC
 */
public class Simple {
    public static void main(String[] args) {
       List<Integer> danhSachSo=new ArrayList<>();  
       danhSachSo.add(3);
       danhSachSo.add(4);
       danhSachSo.add(8);
       danhSachSo.add(11);
       //c1:
        System.out.println("Toan bo danh sach:"+danhSachSo);
        
        for (Integer integer : danhSachSo) {
            if (integer%2==0) {
                System.out.println("So chan la:"+integer);
            }
 
        }
        //c2:use lamda
        danhSachSo.forEach((so) ->{
        if (so%2==0) {
            System.out.println("So chan:"+so);
        }
    });
        
        //check contains or not
        int x=5;
        if (danhSachSo.contains(x)) {
            System.out.println("x have in list");
        }else{
            System.out.println("No have in list!!");
        }
              
    }
   
    
}

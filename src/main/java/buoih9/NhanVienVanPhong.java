/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buoih9;

/**
 *
 * @author PC
 */
public class NhanVienVanPhong {
    protected String totNghiepTruong;
    protected String queQuan;

    public NhanVienVanPhong(String totNghiepTruong, String queQuan) {
        this.totNghiepTruong = totNghiepTruong;
        this.queQuan = queQuan;
    }

    public String getTotNghiepTruong() {
        return totNghiepTruong;
    }

    public void setTotNghiepTruong(String totNghiepTruong) {
        this.totNghiepTruong = totNghiepTruong;
    }

    public String getQueQuan() {
        return queQuan;
    }

    public void setQueQuan(String queQuan) {
        this.queQuan = queQuan;
    }

    
 
}

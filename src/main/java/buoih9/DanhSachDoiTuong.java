/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buoih9;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PC
 */
public class DanhSachDoiTuong {

    public static void main(String[] args) {
        BaoVe baoveNhatBaoVe = new BaoVe("Dai den", 2, "Vo Van Nhat", "23");
        BaoVe baoveAnBaoVe = new BaoVe("Dai xanh", 3, "Nguyen Van An", "3");
        BaoVe baoVeDuBaoVe = new BaoVe("Dai do", 6, "Do Van Dung", "16");

        List<BaoVe> danhSachBaoVe = new ArrayList<>(3);
        danhSachBaoVe.add(baoVeDuBaoVe);
        danhSachBaoVe.add(baoveAnBaoVe);
        danhSachBaoVe.add(baoveNhatBaoVe);
        //will return true
        BaoVe BaoveNhatX = baoveNhatBaoVe;
        //will false
        //Fix:
        //change toString and use hasdCode to make Baove2 have in the list!
        BaoVe baoveNhatBaoVe2 = new BaoVe("Dai den", 2, "Vo Van Nhat", "23");
        System.out.println("Gia tri 1:" + baoveNhatBaoVe);
        System.out.println("Gia tri 2:" + baoveNhatBaoVe2);
        System.out.println("Gia tri 3:" + BaoveNhatX);
        System.out.println("Gia tri 1==Gia tri 2:" + (BaoveNhatX.equals(baoveNhatBaoVe)));
        if (danhSachBaoVe.contains(baoveNhatBaoVe2)) {
            System.out.println("Bao ve nhat co nam trong danh sach");
        } else {
            System.out.println("Bao ve nhat khong nam trong danh sach");
        }
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buoih9;

/**
 *
 * @author PC
 */
public class QuanLyBanHang extends NhanVienVanPhong implements WorkBenefit{
    private String chungchiNghe ;

    public QuanLyBanHang(String chungchiNghe, String totNghiepTruong, String queQuan) {
        super(totNghiepTruong, queQuan);
        this.chungchiNghe = chungchiNghe;
    }

    public String getChungchiNghe() {
        return chungchiNghe;
    }

    public void setChungchiNghe(String chungchiNghe) {
        this.chungchiNghe = chungchiNghe;
    }
 
    @Override
    public String getWorkBenefit() {
        return "Chi tra chi phi di lai";
    }
   

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritanceinjava;

/**
 *
 * @author PC
 */
public class Sub_class extends Super_classs{
    int num=10;

    @Override
    public void display() {
        System.out.println("This is the display method of ");
    }
    public void my_method(){
        Sub_class sub_class=new Sub_class();
        
        sub_class.display();
        super.display();
        System.out.println("value of the variable named num in sub class:"+sub_class);
        System.out.println("value of the variable name num in super class:"+super.num);
    }
    
    
}

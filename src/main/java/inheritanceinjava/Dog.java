/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritanceinjava;

/**
 *
 * @author PC
 */
public class Dog extends Animal{

    @Override
    public void move() {
        System.out.println("Walk and run");
    }
}
 class testDog{
     public static void main(String[] args) {
         Animal a=new Animal();
         Animal b=new Dog();
         
         a.move();
         b.move();
     }
}

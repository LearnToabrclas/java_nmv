/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package abstractclass;

/**
 *
 * @author PC
 */
public class Manager extends Emoloyee{
    public Manager(String firstName, String lastName, String department, double salary) {
        super(firstName, lastName, department, salary);
    }

    @Override
    public void displayInfo() {
        System.out.println("Manager Information:");
        System.out.println("Name: " + getFullName());
        System.out.println("Department: " + super.department);
        System.out.println("Salary: $" + super.salary);
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package abstractclass;

/**
 *
 * @author PC
 */
public abstract class Emoloyee {
     private String firstName;
    private String lastName;
    String department;
    double salary;

    public Emoloyee(String firstName, String lastName, String department, double salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.salary = salary;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public abstract void displayInfo();
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package interfaceịnava;

/**
 *
 * @author PC
 */
public interface CatBehavious {
    //interface da ke thua 
    //Các method được khai báo trong Interface phải là method rỗng.
    //Không thể tạo đối tượng từ Interface.
    //Một Class có thể implement một hoặc nhiều Interface./

    //Cach con meo chay voi toc do
    void run(int speed);

    //Cach con meo bat chuot
    void catchMouse(int mouse);

    //Dinh nghia con meo ngu
    void sleep();

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package interfaceịnava;

import java.util.Scanner;

/**
 *
 * @author PC
 */
public class Edureka implements extInterface{
    @Override
    public void method1() {
        System.out.println("implement method 1:");
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the number to find square root in java:");
        double square=scanner.nextDouble();
        double squareRoot=Math.sqrt(square);
        System.out.printf("Square root of number :%f is:%f %n",square,squareRoot);
    }

    @Override
    public void method2() {
        System.out.println("implement ò method 2");
    }
    public static void main(String[] args) {
        extInterface obj=new Edureka();
        obj.method1();
        obj.method2();
    }
}

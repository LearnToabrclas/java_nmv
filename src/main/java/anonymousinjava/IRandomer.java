/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package anonymousinjava;

/**
 *
 * @author PC
 */
public interface IRandomer {
     public static void main(String[] args) {
        // Tạo một đối tượng lớp nặc danh từ giao diện Runnable
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("Hello from anonymous class!");
            }
        };

        // Chạy phương thức run() của đối tượng lớp nặc danh
        runnable.run();
    }
}

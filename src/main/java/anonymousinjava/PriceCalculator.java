/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package anonymousinjava;

import java.util.List;

/**
 *
 * @author PC
 */
public interface PriceCalculator {
   double calculateTotalPrice(List<Product> products); 
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package anonymousinjava;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PC
 */
public class ShoppingCart {
    private List<Product> products;

    public ShoppingCart() {
        products = new ArrayList<>();
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public void removeProduct(Product product) {
        products.remove(product);
    }

    public double getTotalPrice(PriceCalculator calculator) {
        return calculator.calculateTotalPrice(products);
    }

    public void displayCartItems() {
        System.out.println("Shopping Cart:");
        for (Product product : products) {
            System.out.println(product.getName() + " - $" + product.getPrice());
        }
    }
}



 class Shop {
    public static void main(String[] args) {
        // Tạo sản phẩm
        Product product1 = new Product("Shirt", 29.99);
        Product product2 = new Product("Jeans", 49.99);
        Product product3 = new Product("Shoes", 59.99);

        // Tạo giỏ hàng và thêm sản phẩm
        ShoppingCart cart = new ShoppingCart();
        cart.addProduct(product1);
        cart.addProduct(product2);
        cart.addProduct(product3);

        // Sử dụng phương thức ẩn danh để tính tổng giá theo mức giảm giá 10%
        double totalPrice = cart.getTotalPrice(new PriceCalculator() {
            @Override
            public double calculateTotalPrice(List<Product> products) {
                double total = 0.0;
                for (Product product : products) {
                    total += product.getPrice() * 0.9; // Giảm giá 10%
                }
                return total;
            }
        });

        // Hiển thị giỏ hàng và tổng giá đã tính
        cart.displayCartItems();
        System.out.println("Total Price (with discount): $" + totalPrice);
    }
}

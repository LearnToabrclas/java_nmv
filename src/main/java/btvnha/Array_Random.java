/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package btvnha;

import java.util.Random;

/**
 *
 * @author PC
 */
public class Array_Random {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Step 1:initialize randarray 41 elements and Assigns each element to each elementrandom  
        int[] randArray = randomElementArray();
        //Step 2: Print negative value in above array
        DisplaysNegativeElement(randArray);
        //Step 3: Ilitialize evenNumbers from randaray 
        int[] evenNumbers = evenNumbersFromRandArray(randArray);
        //Step 4:calculate the total prime number of elements in the randarray
        int SumPrime = TotalPrimeElementArray(randArray);

    }

    private static int[] randomElementArray() {
        System.out.println("Random 41 element int array:");
        //Array have 41 elements
        int[] randArray = new int[41];
        // Initialize random to can use random
        Random random = new Random();
        //Assigns each element to each elementrandom 
        for (int i = 0; i < 41; i++) {
            randArray[i] = random.nextInt();

        }
        //Displays values in array
        //Iterate through the array 
        for (int i = 0; i < 41; i++) {
            //Print array
            System.out.println(randArray[i]);
        }
        return randArray;
    }

    private static void DisplaysNegativeElement(int[] randArray) {
        //Displays the message
        System.out.println();
        System.out.println("Negative value element in  randArray:");
        for (int i = 0; i < 41; i++) {
            // Iterrate through array If randArray <0 -> negative Element
            if (randArray[i] < 0) {
                System.out.println(randArray[i]);
            }

        }
    }

    private static int[] evenNumbersFromRandArray(int[] randArray) {
        //find even number of numbers in randArray
        int evenCount = 0;
        for (int i = 0; i < 41; i++) {
            if (randArray[i] % 2 == 0) {
                evenCount++;
            }
        }
        //Initialize []evenNumbers =total even number in randArray
        int[] evenNumbers = new int[evenCount];
        int where = 0;
        for (int i = 0; i < randArray.length; i++) {
            if (randArray[i] % 2 == 0) {
                evenNumbers[where] = randArray[i];
                where++;
            }
        }
        //Display even numbers in array
        System.out.println();
        System.out.println("Even number in array: ");
        for (int i = 0; i < evenNumbers.length; i++) {
            System.out.println(evenNumbers[i]);
        }
        return evenNumbers;

    }

    //Check Prime number
    public static boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        }
        //  A prime number is a number that is divisible by 1 and itself
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    private static int TotalPrimeElementArray(int[] randArray) {

        // System.out.println("Total Prime Element in randArray is:");
        int TotalPrime = 0;
        boolean hasPrimeNumber = false;
        for (int i = 0; i < randArray.length; i++) {
            //check Iterrate through array to find primr number
            if (isPrime(randArray[i])) {
                //Sum all element is prime number in randArray
                TotalPrime += randArray[i];
                hasPrimeNumber = true;
            }
        }
        if (hasPrimeNumber) {
            System.out.println("Sum Prime element in randArray is: " + TotalPrime);
        } else {
            System.out.println("No have Prime in randArray");
        }
        return TotalPrime;
    }
}

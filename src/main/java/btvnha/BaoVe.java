/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package btvnha;

import jdk.nashorn.internal.parser.TokenType;

/**
 *
 * @author PC
 */
public class BaoVe implements Employee {

    protected String Ma;
    protected String ten;
    protected String ngayThangNamSinh;
    protected String chungChiVoThuat;
    protected int soLanMatDo;

    public BaoVe(String Ma, String ten, String ngayThangNamSinh, String chungChiVoThuat) {
        this.Ma = Ma;
        this.ten = ten;
        this.ngayThangNamSinh = ngayThangNamSinh;
        this.chungChiVoThuat = chungChiVoThuat;
    }

    @Override
    public String getworkBenefit() {
        return "chi tra tien an trua va toi";
    }

    @Override
    public String getHieuSuatCongViec() {
        if (soLanMatDo==0) {
            return "Gioi";
        }else if(soLanMatDo<3){
            return "Kha";
        }
        return "Kem";
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package btvnha;

/**
 *
 * @author PC
 */
public class nhanVienKho extends nhanVienBanHang implements Employee {

    protected String tinhTrangSucKhoe;
    protected int ngayKhongChamCong;

    public nhanVienKho(String tinhTrangSucKhoe, String ma, String ten, String ngayThangNamSinh, String totNghiepTruongnao, String queQuan) {
        super(ma, ten, ngayThangNamSinh, totNghiepTruongnao, queQuan);
        this.tinhTrangSucKhoe = tinhTrangSucKhoe;
    }

    @Override
    public String getworkBenefit() {
        return "Chi tra chi phi kham chua benh dinh ky";
    }

    @Override
    public String getHieuSuatCongViec() {
        if (ngayKhongChamCong > 4) {
            return "Kem";
        } else if (ngayKhongChamCong < 3) {
            return "TRung binh";
        } else {
            return "Tot!!";
        }
    }

}

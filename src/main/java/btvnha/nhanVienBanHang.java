/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package btvnha;

/**
 *
 * @author PC
 */
public class nhanVienBanHang implements Employee {

    protected String ma;
    protected String ten;
    protected String ngayThangNamSinh;
    protected String totNghiepTruongnao;
    protected String queQuan;
    protected int soDon1Thang;

    public nhanVienBanHang(String ma, String ten, String ngayThangNamSinh, String totNghiepTruongnao, String queQuan) {
        this.ma = ma;
        this.ten = ten;
        this.ngayThangNamSinh = ngayThangNamSinh;
        this.totNghiepTruongnao = totNghiepTruongnao;
        this.queQuan = queQuan;
    }

    @Override
    public String getworkBenefit() {
        return "Chi tra tien an trua,dien thoai";
    }

    @Override
    public String getHieuSuatCongViec() {
        if (soDon1Thang < 10) {
            return "Kem";
        } else if (soDon1Thang > 15 && soDon1Thang <= 25) {
            return "trung Binh";
        } else {
            return "Tot";
        }
    }

}

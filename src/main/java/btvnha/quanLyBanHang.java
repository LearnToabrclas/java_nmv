/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package btvnha;

/**
 *
 * @author PC
 */
public class quanLyBanHang extends nhanVienBanHang implements Employee {

    protected String danhSachChungChiNghe;
    protected int KPI;

    public quanLyBanHang(String danhSachChungChiNghe, String ma, String ten, String ngayThangNamSinh, String totNghiepTruongnao, String queQuan) {
        super(ma, ten, ngayThangNamSinh, totNghiepTruongnao, queQuan);
        this.danhSachChungChiNghe = danhSachChungChiNghe;
    }

    @Override
    public String getworkBenefit() {
        return "Chi tra chi phi di lai";
    }

    @Override
    public String getHieuSuatCongViec() {
        if (KPI > 200) {
            return "Dat";
        } else {
            return "Chua dat KPI";
        }
    }

}

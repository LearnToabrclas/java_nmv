/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buoi10;

/**
 *
 * @author PC
 */
public class Employe {

    protected String Ma;
    protected String ten;
    protected String ngayThangNamSinh;
    protected String chucVuString;

    public Employe(String Ma, String ten, String ngayThangNamSinh, String chucVuString) {
        this.Ma = Ma;
        this.ten = ten;
        this.ngayThangNamSinh = ngayThangNamSinh;
        this.chucVuString = chucVuString;
    }

    public String getMa() {
        return Ma;
    }

    public void setMa(String Ma) {
        this.Ma = Ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getNgayThangNamSinh() {
        return ngayThangNamSinh;
    }

    public void setNgayThangNamSinh(String ngayThangNamSinh) {
        this.ngayThangNamSinh = ngayThangNamSinh;
    }

    public String getChucVuString() {
        return chucVuString;
    }

    public void setChucVuString(String chucVuString) {
        this.chucVuString = chucVuString;
    }

    @Override
    public String toString() {
        return "Employe{" + "Ma=" + Ma + ", ten=" + ten + ", ngayThangNamSinh=" + ngayThangNamSinh + ", chucVuString=" + chucVuString + '}';
    }
    public void getworkBenefit(){
        //benefit of employee
        System.out.println("Benefit of employee:"+ten);
    }

}

   

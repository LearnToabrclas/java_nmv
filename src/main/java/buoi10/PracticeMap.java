/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buoi10;

import buoih9.NhanVien;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author PC
 */
public class PracticeMap {

    public static void main(String[] args) {
        Map<String, String> tuDienAnhViet = new HashMap<>(10);
        //Copy all DAta from Map dictonanary
        Map<String, String> tuDienAnhViet2 = new HashMap<>(tuDienAnhViet);
        //Khoi tao mot tu dien nhan vien co Key la ma nhan vien
        //Mot doi tuong NhanVien Nham truy van nhanh thong tin cua nhan vien
        //theo ma nhan vien
        Map<Integer, NhanVien> danhSachNhanVien = new HashMap<>();

        //Day cac c ap key-valjue vao trong map
        tuDienAnhViet.put("family", "gia dinh");
        tuDienAnhViet.put("love", "yeu");
        tuDienAnhViet.put("love", "tinh yeu");

        //Kiem tra su ton tai cua mot key nap do trong map
        boolean coChuaFamily = tuDienAnhViet.containsKey("family");
        if (tuDienAnhViet.containsKey("family")) {
            System.out.println("Tu dien co chua Family");
        }

        //lay gia tri tu trong map
        String nghiaLove = tuDienAnhViet.get("love");
        System.out.println("Nghia cua tu lop la:" + nghiaLove);
        //  String nghiaHate = tuDienAnhViet.get("hate");
        //  System.out.println("Nghia cua tu Hate:" + nghiaHate);
        System.out.println("Null exception:" + danhSachNhanVien.get(123).getTenNhanVien());

        //Xoa gia tri ra khoi Map
        String NghiaBiXoa = tuDienAnhViet.remove("love");

        danhSachNhanVien.remove(23);

        //Duyet cac phan tu cua hashMap su dung chuyen doi ve cap Hashset
        for (Map.Entry<String, String> entry
                : tuDienAnhViet.entrySet()) {
            System.out.println("key=" + entry.getKey() + ".value=" + entry.getValue());
        }
        //Duyet cac phan tu thong qua keyset
        for (String key : tuDienAnhViet.keySet()) {
            System.out.println("key=" + key + ", value=" + tuDienAnhViet.get(key));
        }
        //Duyet su dung bieu thuc lamda
        tuDienAnhViet.forEach((key, value)
                -> System.out.println("Key=" + key + ", Value =" + value));

    }
}

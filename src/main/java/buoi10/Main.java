/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buoi10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author PC
 */
public class Main {

    public static void main(String[] args) {
        List<Employe> employeeList = new ArrayList<>();

        //Any 10 employees
        employeeList.add(new Employe("NV001", "Nguyen Van A", "01/09/1992", "Bảo vệ"));
        employeeList.add(new Employe("NV002", "Trinh Hong Anh", "03/05/1993", "Nhân viên bán hàng"));
        employeeList.add(new Employe("NV003", "Nguyen Thi Van Anh", "10/10/2003", "Quản lý bán hàng"));
        employeeList.add(new Employe("NV004", "Le duc Van", "19/06/1989", "Nhân viên kho"));
        employeeList.add(new Employe("NV005", "Giap Nhat ANh", "23/02/1982", "Bảo vệ"));
        employeeList.add(new Employe("NV006", "Nguyen The quang TRuong", "08/09/1992", "Nhân viên bán hàng"));
        employeeList.add(new Employe("NV007", "Dao duc Viet Ha", "07/03/1989", "Quản lý bán hàng"));
        employeeList.add(new Employe("NV008", "Le Duy Dai", "09/02/1993", "Nhân viên kho"));
        employeeList.add(new Employe("NV009", "Chu Quoc Khanh", "2/22/1983", "Bảo vệ"));
        employeeList.add(new Employe("NV010", "Le Yen Nhi", "8/21/1992", "Nhân viên bán hàng"));

        //use HasdMap
          HashMap<String, Integer> count = new HashMap<>();
        // Browse and count the number of employees by type
        //loop from first to last of list
        for (Employe employee : employeeList) {
            String role = employee.getChucVuString();
            //count role of employee
            if (count.containsKey(role)) {
                count.put(role, count.get(role) + 1);
            } else {
                count.put(role, 1);
            }
        }
    //Print the numbers of employees of each type
     for (Map.Entry<String, Integer> entry : count.entrySet()) {
            System.out.println("Number of employees is: " + entry.getKey() + ": " + entry.getValue());
        }
     // Remove security guards from the list
      ArrayList<Employe> listem = new ArrayList<>();
        for (Employe employee : employeeList) {
            if (!employee.getChucVuString().equals("Bảo vệ")) {
                listem.add(employee);
            }
        }
        employeeList = listem;
        
       //// Create a new list with employees born between 1970 and 2000
       listem=new ArrayList<>();
       for (Employe employee : employeeList) {
    int birthYear = Integer.parseInt(employee.getNgayThangNamSinh().substring(6));
    if (birthYear >= 1970 && birthYear <= 2000) {
        listem.add(employee);
    }
}
 
       
       ///// Call the getWorkBenefit function of all objects in the new list
        for (Employe employe : listem) {
            employe.getworkBenefit();
            
        }
    }
}

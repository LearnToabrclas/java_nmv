/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package buoi10;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author PC
 */
public class RandomUnique {
    public static void main(String[] args) {
        List<Integer> danhSach1=new ArrayList<>();
        Random rand=new Random();
        //Danh dau thoi gian bat dau xu ly
        long startTime2=System.currentTimeMillis();
        //hashSet
        Set<Integer> danhSach2=new HashSet<>();
        for (int i = 0; i <100; i++) {
            danhSach2.add(rand.nextInt(300));
            
        }
         long startTime1=System.currentTimeMillis();
        //arraylist
        while (danhSach1.size()<=100) {            
            int randNum=rand.nextInt(300);
            if (!danhSach1.contains(randNum)) {
                danhSach1.add(randNum);
            }
        }
        System.out.println(danhSach1);
        System.out.println("Thoi gian xu ly:"+(System.currentTimeMillis()-startTime2));
        System.out.println(danhSach2);
        System.out.println("Thoi gian xu ly:"+(System.currentTimeMillis()-startTime1));
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package acessmodifiers;

/**
 *
 * @author PC
 */
public class Car {
    private String brand; //=>chỉ trong class dùng được
    protected int price;//=>class,package,subclass
    int year;//=>Không khai thì là default: class,package
    public String color;//=>all

    public Car(String brand, int price, int year, String color) {
        this.brand = brand;
        this.price = price;
        this.year = year;
        this.color = color;
    }

    private void startEngine() {
        System.out.println("Engine started!");
    }

    protected void accelerate() {
        System.out.println("Car is accelerating!");
    }

    void stopEngine() {
        System.out.println("Engine stopped!");
    }

    public void displayInformation() {
        System.out.println("Brand: " + brand);
        System.out.println("Price: " + price);
        System.out.println("Year: " + year);
        System.out.println("Color: " + color);
    }
}

